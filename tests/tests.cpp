#include <cstdio>
#include <sys/time.h>
#include "tests.h"
#include "../utils.h"
#include "../part1/part1.h"
#include "../part2/part2.h"
#include "../part3/part3.h"


void test_diff(const char *file_name, int num_of_threads) {
    int count;
    long positives;
    auto *matrix = read_list(file_name, count, positives);

    double one_thread_result = monte_carlo_calculate(matrix, count, positives);
    printf("One_thread_result: %f\n", one_thread_result);

    double pthread_result = solve_with_pthread(num_of_threads, matrix, count, positives);
    printf("Pthread_result: %f\n", pthread_result);

    double open_mp_result = solve_with_open_mp(num_of_threads, matrix, count, positives);
    printf("Open_mp_result: %f\n", open_mp_result);

    printf("Diff one&pthread: %f\n", one_thread_result - pthread_result);
    printf("Diff one&omp: %f\n", one_thread_result - open_mp_result);
    printf("Diff pthread&omp: %f\n", pthread_result - open_mp_result);
}

void test_time(const char *file_name, int num_of_threads, bool with_one) {
    struct timeval time_start{};
    struct timeval time_finish{};
    long one_thread_time;
    long pthread_time;
    long open_mp_time;
    int count;
    long positives;
    auto *matrix = read_list(file_name, count, positives);

    if (with_one) {
        time_start.tv_sec = 0;
        time_start.tv_usec = 0;
        time_finish.tv_sec = 0;
        time_finish.tv_usec = 0;

        gettimeofday(&time_start, nullptr);
        monte_carlo_calculate(matrix, count, positives);
        gettimeofday(&time_finish, nullptr);
        one_thread_time = (time_finish.tv_sec - time_start.tv_sec) * 1000000 + time_finish.tv_usec - time_start.tv_usec;
        printf("One_thread_time: %ld\n", one_thread_time);
    }

    time_start.tv_sec = 0;
    time_start.tv_usec = 0;
    time_finish.tv_sec = 0;
    time_finish.tv_usec = 0;

    gettimeofday(&time_start, nullptr);
    solve_with_pthread(num_of_threads, matrix, count, positives);
    gettimeofday(&time_finish, nullptr);
    pthread_time = (time_finish.tv_sec - time_start.tv_sec) * 1000000 + time_finish.tv_usec - time_start.tv_usec;
    printf("Pthread_time: %ld\n", pthread_time);

    time_start.tv_sec = 0;
    time_start.tv_usec = 0;
    time_finish.tv_sec = 0;
    time_finish.tv_usec = 0;

    gettimeofday(&time_start, nullptr);
    solve_with_open_mp(num_of_threads, matrix, count, positives);
    gettimeofday(&time_finish, nullptr);
    open_mp_time = (time_finish.tv_sec - time_start.tv_sec) * 1000000 + (time_finish.tv_usec - time_start.tv_usec);
    printf("Open_mp_time: %ld\n", open_mp_time);
}