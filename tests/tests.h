
#ifndef PARALLEL_TESTS_H
#define PARALLEL_TESTS_H

void test_diff(const char *file_name, int num_of_threads);

void test_time(const char *file_name, int num_of_threads, bool with_one);

#endif //PARALLEL_TESTS_H
