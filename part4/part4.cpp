#include <ctime>
#include "../part1/part1.h"
#include "../part2/part2.h"
#include "../part3/part3.h"


long count_time(int part_number,
                int pthreads_num,
                Circle *mas,
                int count,
                long positives) {
    struct timespec time_start{};
    struct timespec time_finish{};

    switch (part_number) {
        case 1:
            clock_gettime(CLOCK_REALTIME, &time_start);
            monte_carlo_calculate(mas, count, positives);
            clock_gettime(CLOCK_REALTIME, &time_finish);
            break;
        case 2:
            clock_gettime(CLOCK_REALTIME, &time_start);
            solve_with_pthread(pthreads_num, mas, count, positives);
            clock_gettime(CLOCK_REALTIME, &time_finish);
            break;
        case 3:
            clock_gettime(CLOCK_REALTIME, &time_start);
            solve_with_open_mp(pthreads_num, mas, count, positives);
            clock_gettime(CLOCK_REALTIME, &time_finish);
            break;
        default:
            return 0;
    }

    return time_finish.tv_nsec - time_start.tv_nsec;
}