
#ifndef COURSE_PART4_H
#define COURSE_PART4_H

long count_time(int part_number,
                int pthreads_num,
                Circle *mas,
                int count,
                long positives);

#endif //COURSE_PART4_H
