
#ifndef COURSE_UTILS_H
#define COURSE_UTILS_H

#include <cstdio>

class Point {
public:
    int x;
    int y;
};

class Circle {
public:
    Point cen;
    int rad;
};

int read_number(FILE *file_d);

Circle *read_list(const char *file_name, int &count, long &positives);

#endif //COURSE_UTILS_H
