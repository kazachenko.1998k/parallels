
#ifndef COURSE_PART3_H
#define COURSE_PART3_H

#include "../utils.h"

double solve_with_open_mp(int num_of_threads,
                          Circle *mas,
                          int count,
                          long positives);

#endif //COURSE_PART3_H
