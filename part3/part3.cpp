#include "part3.h"
#include <omp.h>
#include "../part1/part1.h"

double solve_with_open_mp(
        int num_of_threads,
        Circle *mas,
        int count,
        long positives
) {
    double result = 0;
#pragma omp parallel for reduction (+: result)
    for (int i = 0; i < num_of_threads; ++i) {
        result += monte_carlo_calculate(mas, count, positives / num_of_threads);
    }

    return result / num_of_threads;
}