#include <cmath>
#include "part1.h"
#include <iostream>
#include <ctime>

//Определение площади квадрата вокруг круга.
void calculate_max_min(int &maxx, int &minx, int &maxy, int &miny, Circle *mas, int count) {
    maxx = mas[0].cen.x + mas[0].rad;
    minx = mas[0].cen.x - mas[0].rad;
    maxy = mas[0].cen.y + mas[0].rad;
    miny = mas[0].cen.y - mas[0].rad;
    for (int i = 1; i < count; i++) {
        int z = mas[i].cen.x + mas[i].rad;
        if (maxx < z)
            maxx = z;
        z = mas[i].cen.x - mas[i].rad;
        if (minx > z)
            minx = z;
        z = mas[i].cen.y + mas[i].rad;
        if (maxy < z)
            maxy = z;
        z = mas[i].cen.y - mas[i].rad;
        if (miny > z)
            miny = z;
    }
}

double monte_carlo_calculate(Circle *mas, int count, long positives) {
    srand(time(nullptr) ^ (int) pthread_self());

    int maxx, minx, maxy, miny;
    calculate_max_min(maxx, minx, maxy, miny, mas, count);
    long sum = 0;
    for (long i = 0; i < positives; i++) {
        double xx = rand();
        double yy = rand();
        xx = xx / RAND_MAX * (maxx - minx) + minx;
        yy = yy / RAND_MAX * (maxy - miny) + miny;
        bool t = false;
        for (int j = 0; j < count; j++) {
            double len = (xx - mas[j].cen.x) * (xx - mas[j].cen.x) + (yy - mas[j].cen.y) * (yy - mas[j].cen.y);
            len = sqrt(len);
            if (len < mas[j].rad)
                t = true;
        }
        if (t)
            sum++;
    }
    double percent = (double) (sum) / (double) (positives);
    int sqrt = (maxx - minx) * (maxy - miny);
    double s = percent * sqrt;
    return s;
}