#include "../utils.h"

#ifndef COURSE_PART1_H
#define COURSE_PART1_H

double monte_carlo_calculate(Circle *mas, int count, long positives);

#endif //COURSE_PART1_H
