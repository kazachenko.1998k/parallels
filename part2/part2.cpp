#include <pthread.h>
#include <cstring>
#include "part2.h"
#include "../utils.h"
#include "../part1/part1.h"
#include "pthread_arg.h"
#include <ctime>

void *thread(void *arg) {
    auto *result = (double *) malloc(sizeof(double));
    struct pthread_arg *pthread_arg = ((struct pthread_arg *) arg);
    *result = monte_carlo_calculate(pthread_arg->mas, pthread_arg->count, pthread_arg->positives);
    return result;
}

double solve_with_pthread(int pthreads_num, Circle *mas,
                          int count,
                          long positives) {
    auto *args = new pthread_arg[pthreads_num];
    void *thread_result;
    pthread_t pthreads[pthreads_num];
    double result = 0;

    // run pthreads
    srand(time(nullptr));
    for (int i = 0; i < pthreads_num; ++i) {
        args[i] = {mas, count, positives / pthreads_num};
        pthread_create(&pthreads[i], nullptr, thread, &args[i]);
    }

    // join all pthreads
    for (int i = 0; i < pthreads_num; ++i) {
        pthread_join(pthreads[i], &thread_result);
        double dbl = *(double *) thread_result;
        result += dbl;
        free(thread_result);
    }

    return result / pthreads_num;
}