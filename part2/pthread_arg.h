#ifndef COURSE_PTHREAD_ARG_H
#define COURSE_PTHREAD_ARG_H

#include <stdlib.h>

class pthread_arg {
public:
    Circle *mas;
    int count;
    long positives;
};

#endif //COURSE_PTHREAD_ARG_H
