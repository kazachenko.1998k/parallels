
#ifndef COURSE_PART2_H
#define COURSE_PART2_H

#include "../utils.h"

void *thread(void *arg);

double solve_with_pthread(
        int pthreads_num, Circle *mas,
        int count,
        long positives
);

#endif //COURSE_PART2_H
