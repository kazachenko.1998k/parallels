#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include "utils.h"
#include "part1/part1.h"
#include "part2/part2.h"
#include "part3/part3.h"
#include "part4/part4.h"
#include "tests/tests.h"

// проверка аргумантов командной строки
void check_args(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Usage: type file [num of threads]\n");
    }
}

int get_num_of_pthreads(int argc, char *argv[]) {
    if (argc == 4) {
        return atoi(argv[3]);
    }
    return 1;
}

int main() {

//    if (strncmp(argv[1], "s", (int) 3) == 0) {
//        printf("%f", monte_carlo_calculate(matrix, count, positives));
//    } else if (strncmp(argv[1], "p", (int) 3) == 0) {
//        printf("\nsolve_with_pthread: %f\n", solve_with_pthread(num_of_pthreads, matrix, count, positives));
//    } else if (strncmp(argv[1], "m", (int) 3) == 0) {
//        printf("\nsolve_with_open_mp: %f\n", solve_with_open_mp(num_of_pthreads, matrix, count, positives));
//    } else if (strncmp(argv[1], "cs", (int) 3) == 0) {
//        printf("%ld", count_time(1, num_of_pthreads, matrix, count, positives));
//    } else if (strncmp(argv[1], "cp", (int) 3) == 0) {
//        printf("\ncount_time: %ld\n", count_time(2, num_of_pthreads, matrix, count, positives));
//    } else if (strncmp(argv[1], "cm", (int) 3) == 0) {
//        printf("%ld", count_time(3, num_of_pthreads, matrix, count, positives));
//    } else if (strncmp(argv[1], "td", (int) 3) == 0) {
//        test_diff(filename, num_of_pthreads);
//    } else if (strncmp(argv[1], "tt", (int) 3) == 0) {
//    auto *list_tests = new std::string[18];
//    list_tests[0] = "all_in_one";
//    list_tests[1] = "all_in_one_m";
//    list_tests[2] = "all_in_one_s";
//    list_tests[3] = "big";
//    list_tests[4] = "big_m";
//    list_tests[5] = "big_s";
//    list_tests[6] = "default";
//    list_tests[7] = "default_m";
//    list_tests[8] = "default_s";
//    list_tests[9] = "only_one";
//    list_tests[10] = "only_one_m";
//    list_tests[11] = "only_one_s";
//    list_tests[12] = "with_one_distant";
//    list_tests[13] = "with_one_distant_m";
//    list_tests[14] = "with_one_distant_s";
//    list_tests[15] = "without_merge";
//    list_tests[16] = "without_merge_m";
//    list_tests[17] = "without_merge_s";
    std::string path = R"(C:\Users\Konstantin\CLionProjects\course\tests\resources\only_one)";
//    for (int i = 0; i <= 17; i++) {
//        for (int j = 2; j <= 16; j *= 2) {
//            printf("\n%s: %i\n", list_tests[i].c_str(), j);
            test_time((path ).c_str(), 16, true);
//        }
//    }
    return 0;
}




