#include <cstdio>
#include <cstdlib>
#include "utils.h"


// чтение очередного числа из файла
int read_number(FILE *file_d) {
    int result = -1;
    int number = fgetc(file_d);
    int is_negative = number == '-' ? 1 : 0;

    while (number == ' ') {
        number = fgetc(file_d);
    }

    while (number != EOF && number != '\n' && number != ' ') {
        if (result == -1) {
            result = 0;
        }

        result = result * 10 + atoi((char *) &number);
        number = fgetc(file_d);
    }

    return is_negative ? result * -1 : result;
}

// чтение элементов из файла
Circle *read_list(const char *file_name, int &count, long &positives) {
    FILE *file_d = fopen(file_name, "r");
    if (file_d == nullptr) {
        printf("Error open file!");
    }

    count = read_number(file_d);

    positives = read_number(file_d);

    auto *mas = new Circle[count];
    for (int i = 0; i < count; ++i) {
        mas[i].cen = {read_number(file_d), read_number(file_d)};
        mas[i].rad = read_number(file_d);
    }

    fclose(file_d);

    return mas;
}

